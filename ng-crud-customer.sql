-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 05, 2017 at 05:31 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ng-crud-customer`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `First_name` varchar(255) NOT NULL,
  `Last_name` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `Username`, `First_name`, `Last_name`, `Email`, `Status`) VALUES
(1, 'sahan', 'anjana', 'sahan', 'wsa', 'live'),
(4, 'anjana', 'Wijesunadara', 'sissanayaka', 'wasnajana951', 'Pending'),
(5, 'anjanaw', 'sahan', 'wijesundara', 'yahoo.com', 'live'),
(7, 'aryanaw', 'Aryana', 'Wijesundara', 'aryana@gmaill.com', 'Pending'),
(8, 'nameone', 'Name', 'One', 'nameone@gmail.com', 'Name one'),
(9, 'usertwo', 'User', 'Two', 'usertwo@gmail.com', 'User Two is Successf'),
(10, 'jhon', 'John', 'Mas', 'mas@gmai.com', 'dfdf'),
(11, 'Hello', 'Wrold', 'wo', 'wo@gmail.com', 'dfd'),
(12, 'naungnaung', 'Naung', 'Naung', 'naung@gmail.com', 'Naung'),
(13, 'mrcheft', 'Mr', 'Cheft', 'cheft@gmail.com', 'Hello');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
